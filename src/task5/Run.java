package task5;

public class Run {

	public static void main(String[] args) throws Exception {
        // input arguments
		String msg1 = "Booking london novak" ;
        String msg2 = "New trip prague" ;
        String msg3 = "New trip budejovice" ;
        String msg4 = "Random test" ;
        String msg5 = "Booking adsfg" ;
        String queueName = "jms/mdw-allordersqueue" ;

        // create the producer object and send the message
        JMSProducer producer = new JMSProducer();
        producer.send(queueName, msg1);
        producer.send(queueName, msg2);
        producer.send(queueName, msg3);
        producer.send(queueName, msg4);
        producer.send(queueName, msg5);
    }

}
