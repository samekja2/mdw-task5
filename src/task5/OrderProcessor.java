package task5;


import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;

import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class OrderProcessor implements MessageListener {
 
    // connection factory
    private QueueConnectionFactory qconFactory;
 
    // connection to a queue
    private QueueConnection qcon;
 
    // session within a connection
    private QueueSession qsession;
 
    // queue receiver that receives a message to the queue
    private QueueReceiver qreceiver;
 
    // queue where the message will be sent to
    private Queue allOrdersQueue;
    private Queue bookingsQueue;
    private Queue tripsQueue;
 
    private TextMessage msgBooking;
    private TextMessage msgTrip;
    
 // queue sender that sends a message to the queue
    private QueueSender qsenderBookings;
    private QueueSender qsenderTrips;
    
    // callback when the message exist in the queue
    public void onMessage(Message msg) {
        try {
            String msgText;
            if (msg instanceof TextMessage) {
                msgText = ((TextMessage) msg).getText();
            } else {
                msgText = msg.toString();
            }
            System.out.print("Message Received: " + msgText);
            if (msgText.startsWith("Booking")) {
            	msgBooking.setText(msgText);
            	qsenderBookings.send(msgBooking);
            	System.out.println("; Message forwarded to bookings processor.");
            }
            else if (msgText.startsWith("New trip")) {
            	msgTrip.setText(msgText);
            	qsenderTrips.send(msgTrip);
            	System.out.println("; Message forwarded to trip processor.");
            }
            else {
            	System.out.println("; Unrecognized message type.");
            }
            
            
            
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        }
    }
 
    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String allordersQueueName, String bookingsQueueName, String tripsQueueName)
            throws NamingException, JMSException {
 
        qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        allOrdersQueue = (Queue) ctx.lookup(allordersQueueName);
        bookingsQueue = (Queue) ctx.lookup(bookingsQueueName);
        tripsQueue = (Queue) ctx.lookup(tripsQueueName);
 
        qreceiver = qsession.createReceiver(allOrdersQueue);
        qreceiver.setMessageListener(this);
 
     // create sender and message
        qsenderBookings = qsession.createSender(bookingsQueue);
        qsenderTrips = qsession.createSender(tripsQueue);
        msgBooking = qsession.createTextMessage();
        msgTrip = qsession.createTextMessage();
        
        qcon.start();
    }
 
    // close sender, connection and the session
    public void close() throws JMSException {
    	qsenderBookings.close();
    	qsenderTrips.close();
        qreceiver.close();
        qsession.close();
        qcon.close();
    }
 
    // start receiving messages from the queue
    public void receive(String allordersQueueName, String bookingsQueueName, String tripsQueueName) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);
 
        InitialContext ic = new InitialContext(env);
 
        init(ic, allordersQueueName, bookingsQueueName, tripsQueueName);
        
 
        System.out.println("Connected to " + allordersQueueName.toString() + ", receiving messages...");
        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            close();
            System.out.println("Finished.");
        }
    }
 
    public static void main(String[] args) throws Exception {
        // input arguments
        String allordersQueueName = "jms/mdw-allordersqueue" ;
        String bookingsQueueName = "jms/mdw-bookingsqueue" ;
        String tripsQueueName = "jms/mdw-tripsqueue" ;
 
        // create the producer object and receive the message
        OrderProcessor orderprocessor = new OrderProcessor();
        orderprocessor.receive(allordersQueueName, bookingsQueueName, tripsQueueName);
    }
}

